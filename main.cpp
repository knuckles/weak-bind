#include <iostream>
#include <functional>
#include <memory>
#include "weak_bind.hpp"

struct Test {
  void print(const std::string& s) {
    std::cout << s << std::endl;
  }

  int plus(int i, int j) {
    return i + j;
  }
};

void print(const std::string& s) {
  std::cout << s << std::endl;
}

int plus(int i, int j) {
  return i + j;
}

int main() {
  std::shared_ptr<Test> p(new Test);
  std::weak_ptr<Test> w(p);

  auto test_print = weak_bind(w, &Test::print, p.get(), std::placeholders::_1);
  auto free_print = weak_bind(w, &print, std::placeholders::_1);
  auto free_plus = weak_bind(w, &plus, 1, std::placeholders::_1);

  test_print("test_print");
  free_print("free_print");
  int res = free_plus(2);
  std::cout << "free_plus(2) == " << res << std::endl;

  p.reset();
  std::cout << "---- p.reset(); ----" << std::endl;

  test_print("test_print");
  free_print("free_print");
  res = free_plus(2);
  std::cout << "free_plus(2) == " << res << std::endl;
}
