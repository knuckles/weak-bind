#ifndef WEAK_BIND_HPP
#define WEAK_BIND_HPP

#include <functional>
#include <memory>

template <typename F, typename T>
struct Invoker {
  typedef std::weak_ptr<T> P;

  Invoker(F func, P ptr)
    : func_(func),
      ptr_(ptr) {
  }

  template <typename... Args>
  typename std::result_of<F(Args...)>::type operator()(Args&&... args) {
    typedef typename std::result_of<F(Args...)>::type R;
    if (ptr_.lock())
      return func_(std::forward<Args>(args)...);
    return R();
  }

private:
  F func_;
  P ptr_;
};

template <typename F, typename T, typename... Args>
struct _Bind_helper {
  typedef Invoker<decltype( std::bind(std::declval<F>(), std::declval<Args>()...) ), T> InvokerType;
};

template <typename F, typename T, typename... Args>
typename _Bind_helper<F, T, Args...>::InvokerType
weak_bind(std::weak_ptr<T> ptr, F func, Args&&... args) {
  typedef typename _Bind_helper<F, T, Args...>::InvokerType R;
  return R(std::bind(std::forward<F>(func), std::forward<Args>(args)...), ptr);
}

#endif  // WEAK_BIND_HPP
